package com.example.ventasdomicilio.presenter;

import com.example.ventasdomicilio.model.LoginInteractor;
import com.example.ventasdomicilio.mvp.LoginMVP;

public class LoginPresenter implements LoginMVP.Presenter {

    private LoginMVP.View view;
    private LoginMVP.Model model;

    public LoginPresenter(LoginMVP.View view) {
        this.view = view;
        this.model = new LoginInteractor(); //por ahora

    }

    @Override

    public void onLoginClick() {
        boolean error = false;
        LoginMVP.LoginInfo loginInfo = view.getLoginInfo();

        //Validar datos
        view.showEmailError("");
        view.showPasswordError("");
        if (LoginMVP.LoginInfo.getEmail().isEmpty()) {
            view.showEmailError("El Correo Electronico es Obligatorio");
            error = true;
        } else if (!isEmailValid(LoginMVP.LoginInfo.getEmail())) {
            view.showEmailError("El Correo Electronico No es Valido");
            error = true;
        }

        if (LoginMVP.LoginInfo.getPassword().isEmpty()) {
            view.showPasswordError("La Contraseña es Obligatoria");
            error = true;
        } else if (!isPasswordValid(LoginMVP.LoginInfo.getPassword())) {
            view.showPasswordError("Contraseña no cumple Criterios de seguridad");
            error = true;
        }

        if (!error) {
            view.showProgressBar();
            new Thread(() -> {
                model.validateCredentials(LoginMVP.LoginInfo.getEmail(), LoginMVP.LoginInfo.getPassword(),
                        new LoginMVP.Model.validateCredencialsCallback() {
                            @Override
                            public void onSuccess() {
                                view.getActivity().runOnUiThread(() -> {
                                view.hideProgressBar();
                                view.showPaymentsActivity();
                                });
                            }

                            @Override
                            public void onFailure() {
                                view.getActivity().runOnUiThread(() -> {
                                view.hideProgressBar();
                                view.showGenerarError("Credenciales Invalidas");
                                });
                            }
                        });
            }).start();




        }
    }


    private boolean isEmailValid(String email) {
        return email.contains("@") &&
                (email.endsWith(".com") || email.endsWith(".co"));
    }

    private boolean isPasswordValid(String password) {
        return password.length() >= 8;
    }


    @Override
    public void onFacebookClick() {

    }

    @Override
    public void onGoogleClick() {

    }

}
