package com.example.ventasdomicilio.presenter;

import com.example.ventasdomicilio.model.PaymentsInteractor;
import com.example.ventasdomicilio.mvp.PaymentsMVP;

import java.util.List;

public class PaymentsPresenter implements PaymentsMVP.Presenter {

    private  PaymentsMVP.View view;
    private  PaymentsMVP.Model model;

    public PaymentsPresenter(PaymentsMVP.View view) {
        this.view = view;
        this.model = new PaymentsInteractor();
    }

    @Override
    public void loadPayments() {
        view.showProgressBar();
        new Thread(() -> {
            model.loadPayments(new PaymentsMVP.Model.LoadPaimentsCallback() {
                @Override
                public void setPayments(List<PaymentsMVP.PaymentsDto> payments) {
                      view.getActivity().runOnUiThread(() -> {
                          view.hideProgressBar();
                          view.showPayments(payments);
                      });
                }
            });

        }).start();

    }

    @Override
    public void onPaymentsClick() {

    }

    @Override
    public void onNewSaleClick() {

    }
}
