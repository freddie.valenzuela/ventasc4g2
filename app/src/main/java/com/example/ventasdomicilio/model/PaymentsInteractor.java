package com.example.ventasdomicilio.model;

import com.example.ventasdomicilio.mvp.PaymentsMVP;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PaymentsInteractor implements PaymentsMVP.Model {

    private List<PaymentsMVP.PaymentsDto> payments;
    public PaymentsInteractor() {
        payments = Arrays.asList(
                new PaymentsMVP.PaymentsDto("Cesar Diaz", "Calle 1 # 7 - 53"),
                new PaymentsMVP.PaymentsDto("Freddie Valenzuela", "Calle 2 # 15 - 23"),
                new PaymentsMVP.PaymentsDto("Orlando Sedano", "Calle 3 # 5 - 55"),
                new PaymentsMVP.PaymentsDto("Alexander Carrillo", "Cra 21 # 3 - 53"),
                new PaymentsMVP.PaymentsDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentsDto("Elizabeth Borja", "Urb El Corral mz a cs 1"),
                new PaymentsMVP.PaymentsDto("Freddie Valenzuela", "Calle 2 # 15 - 23"),
                new PaymentsMVP.PaymentsDto("Orlando Sedano", "Calle 3 # 5 - 55"),
                new PaymentsMVP.PaymentsDto("Alexander Carrillo", "Cra 21 # 3 - 53"),
                new PaymentsMVP.PaymentsDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentsDto("Elizabeth Borja", "Urb El Corral mz a cs 1"),
                new PaymentsMVP.PaymentsDto("Freddie Valenzuela", "Calle 2 # 15 - 23"),
                new PaymentsMVP.PaymentsDto("Orlando Sedano", "Calle 3 # 5 - 55"),
                new PaymentsMVP.PaymentsDto("Alexander Carrillo", "Cra 21 # 3 - 53"),
                new PaymentsMVP.PaymentsDto("Cristian Loaiza", "Cra 22 # 7 - 56"),
                new PaymentsMVP.PaymentsDto("Elizabeth Borja", "Urb El Corral mz a cs 1"),
                new PaymentsMVP.PaymentsDto("Frank Polania", "Urb El Corral mz a cs 2")
                );
    }

    @Override
    public void loadPayments(LoadPaimentsCallback callback) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        callback.setPayments(payments);
    }
}
