package com.example.ventasdomicilio.model;

import com.example.ventasdomicilio.mvp.LoginMVP;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LoginInteractor implements LoginMVP.Model {

    private Map<String, String> users;

    public LoginInteractor() {
        users = new HashMap<>();
        users.put("cdiaz@email.com", "12345678");
        users.put("test@email.co", "12345678");
        users.put("a@b.co", "987654321");

    }

    @Override
    public void validateCredentials(String email, String password,
                                    validateCredencialsCallback callback) {
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        if (users.get(email) != null &&
                Objects.equals(users.get(email), password)) {
            callback.onSuccess();
        } else {
            callback.onFailure();
        }
    }
}
