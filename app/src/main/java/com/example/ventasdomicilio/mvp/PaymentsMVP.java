package com.example.ventasdomicilio.mvp;

import android.app.Activity;

import java.util.List;

public interface PaymentsMVP {
    interface Model {
        void loadPayments(LoadPaimentsCallback callback);
        interface LoadPaimentsCallback {
            void setPayments(List<PaymentsDto> payments);
        }

    }

    interface Presenter {
        void loadPayments();
        void onPaymentsClick();
        void onNewSaleClick();

    }

    interface View {
        Activity getActivity();
        void showProgressBar();
        void hideProgressBar();

        void showPayments(List<PaymentsDto> payments);
    }
    class PaymentsDto {
        private String client;
        private String address;


        public PaymentsDto(String client, String address) {
            this.client = client;
            this.address = address;
        }
        

        public String getClient() {
            return client;
        }

        public String getAddress() {
            return address;
        }

    }
    
    
}
