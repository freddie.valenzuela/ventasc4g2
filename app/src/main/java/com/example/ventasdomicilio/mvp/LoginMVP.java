package com.example.ventasdomicilio.mvp;

import android.app.Activity;

public interface LoginMVP {
    interface Model {


        void validateCredentials(String email, String password,
                                 validateCredencialsCallback callback);

        interface validateCredencialsCallback {
            void onSuccess();

            void onFailure();
        }

    }

    interface Presenter {
        void onLoginClick();

        void onFacebookClick();

        void onGoogleClick();

    }

    interface View {
        Activity getActivity();

        LoginInfo getLoginInfo();
        void showEmailError(String error);
        void showPasswordError(String error);

        void showPaymentsActivity();

        void showGenerarError(String error);

        void showProgressBar();

        void hideProgressBar();
    }

    class LoginInfo {
        private static String email;
        private static String password;


        public LoginInfo(String email, String password) {
            this.email = email;
            this.password = password;
        }

        public static String getEmail() {
            return email;
        }

        public static String getPassword() {
            return password;
        }
    }
}
