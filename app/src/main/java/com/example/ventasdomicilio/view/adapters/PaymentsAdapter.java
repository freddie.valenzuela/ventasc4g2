package com.example.ventasdomicilio.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ventasdomicilio.R;
import com.example.ventasdomicilio.mvp.PaymentsMVP;

import java.util.ArrayList;
import java.util.List;

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {

    private List<PaymentsMVP.PaymentsDto> data;

    public PaymentsAdapter() {
        this.data = new ArrayList<>();
    }

    public void setData(List<PaymentsMVP.PaymentsDto> data) {
        this.data = data;
        //notifyDataSetChanged();
    }

    @NonNull
    @Override

    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_payments, parent, false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull PaymentsAdapter.ViewHolder holder, int position) {
        holder.getTvName().setText(data.get(position).getClient());
        holder.getTvAddress().setText(data.get(position).getAddress());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class  ViewHolder extends RecyclerView.ViewHolder {

        private  ImageView ivClient;
        private  TextView tvName;
        private  TextView tvAddress;


        public ViewHolder(View view) {
            super(view);

            initUI(view);

        }

        private void initUI(View view) {
            ivClient = view.findViewById(R.id.iv_client);
            tvName = view.findViewById(R.id.tv_name);
            tvAddress = view.findViewById(R.id.tv_address);
        }

        public ImageView getIvClient() {
            return ivClient;
        }

        public TextView getTvName() {
            return tvName;
        }

        public TextView getTvAddress() {
            return tvAddress;
        }
    }
}
