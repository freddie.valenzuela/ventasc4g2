package com.example.ventasdomicilio.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ventasdomicilio.R;
import com.example.ventasdomicilio.mvp.PaymentsMVP;
import com.example.ventasdomicilio.presenter.PaymentsPresenter;
import com.example.ventasdomicilio.view.adapters.PaymentsAdapter;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.progressindicator.LinearProgressIndicator;

import java.util.ArrayList;
import java.util.List;

public class PaymentsActivity extends AppCompatActivity implements PaymentsMVP.View {

    private LinearProgressIndicator pbWaite;

    private DrawerLayout drawerLayout;
    private MaterialToolbar toolbar;
    private NavigationView navigationView;

    private RecyclerView rvPayments;
    private FloatingActionButton btnNewSale;

    private PaymentsMVP.Presenter presenter;
    private PaymentsAdapter paymentsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        presenter = new PaymentsPresenter(PaymentsActivity.this);
        initUI();
        presenter.loadPayments();

    }

    private void initUI() {
        drawerLayout = findViewById(R.id.drawer_layout);

        toolbar = findViewById(R.id.app_toolbar);
        toolbar.setNavigationOnClickListener(v -> drawerLayout.openDrawer(navigationView));

        pbWaite= findViewById(R.id.pb_wait);

        //navigationView = findViewById(R.id.nv_payments);
        //navigationView.setNavigationItemSelectedListener(this::onMenuItemClick);

        rvPayments= findViewById(R.id.rv_payments);
        rvPayments.setLayoutManager(new LinearLayoutManager(PaymentsActivity.this));
        paymentsAdapter = new PaymentsAdapter();
        rvPayments.setAdapter(paymentsAdapter);

        btnNewSale = findViewById(R.id.btn_new_sale);
        btnNewSale.setOnClickListener(v -> onNewSaleClick());


    }

    private boolean onMenuItemClick(MenuItem menuItem) {
        menuItem.setChecked(true);
        drawerLayout.closeDrawers();
        switch (menuItem.getItemId()){
            case R.id.item2: //Ventas
               // startActivity(new Intent(PaymentsActivity.this, SalesActivity.class));
                break;
        }
        return true;
    }

    private void onNewSaleClick(){
        startActivity(new Intent(PaymentsActivity.this, NewSaleActivity.class));
    }

    @Override
    public Activity getActivity() {
        return PaymentsActivity.this;
    }

    @Override
    public void showProgressBar() {
        pbWaite.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        pbWaite.setVisibility(View.GONE);
    }

    @Override
    public void showPayments(List<PaymentsMVP.PaymentsDto> payments) {
         paymentsAdapter.setData(payments);
    }

}